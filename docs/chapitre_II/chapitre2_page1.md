---
author: Vous
title: A. Une République divisée ? (1792-1793)
---


## Synthèse élèves 6

texte 1

???+ question "Mon exercice"

    **1.** Question 1.   
    Quelle était la couleur du cheval blanc d'Henri IV?

    ??? success "Solution"

        La couleur du cheval blanc d'Henri IV est le blanc. En fait  la robe du cheval d'Henri IV était probablement grise.

    **2.** Question 2.   
    Quelle est la proportion de la terre recouverte par les océans ?

    ??? success "Solution"

        Environ 71 %.

        Pour approfondir : [Lien wikipédia sur les océans](https://fr.wikipedia.org/wiki/Oc%C3%A9an){ .md-button target="_blank" rel="noopener" }